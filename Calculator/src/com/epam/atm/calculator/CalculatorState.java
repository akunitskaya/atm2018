package com.epam.atm.calculator;

public enum CalculatorState {
    NEW_CALCULATION,
    CONTINUE_CALCULATION
}
