package com.epam.atm.calculator;

import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {

        calculatorRunLoop(CalculatorState.NEW_CALCULATION, 0);
    }

    /**
     * Start a new calculation and continue it after receiving the calculation result
     *
     * @param calculatorState - NEW_CALCULATION - when the program is run for the 1st time,
     *                        - CONTINUE_CALCULATION - when the result of the previous calculation is received
     * @param leftNumber      - the 2nd number in operation
     */
    public static void calculatorRunLoop(CalculatorState calculatorState, double leftNumber) {
        CalculatorOperation operation;
        double rightNumber = 0;

        switch (calculatorState) {
            case NEW_CALCULATION:
                leftNumber = readNumberFromKeyboard("the first number");
                operation = readOperationFromKeyboard();
                rightNumber = readNumberFromKeyboard("the second");
                break;
            case CONTINUE_CALCULATION:
                operation = readOperationFromKeyboard();
                rightNumber = readNumberFromKeyboard("the second");
                break;
            default:
                System.out.println("Invalid state, stopping the current calculation...");
                return;
        }

        double result = calculate(leftNumber, operation, rightNumber);
        System.out.println("The operation result is: " + result);

        if (isContinuingCalculation(result)) {
            calculatorRunLoop(CalculatorState.CONTINUE_CALCULATION, result);
        }
    }

    /**
     * Perform calculation with 2 numbers and an operation
     *
     * @param leftNumber  - the 1st number
     * @param operation   - calculation operation
     * @param rightNumber - the 2nd number
     * @return calculation result
     */

    public static double calculate(double leftNumber, CalculatorOperation operation, double rightNumber) {
        switch (operation) {
            case ADDITION:
                return leftNumber + rightNumber;
            case SUBTRACTION:
                return leftNumber - rightNumber;
            case MULTIPLICATION:
                return leftNumber * rightNumber;
            case DIVISION:
                return leftNumber / rightNumber;
            default: //Not possible for the current implementation but will be handled just in case
                System.out.println("Bad operation, returning an empty result");
                return 0;
        }
    }

    /**
     * Read the calculation operation from keyboard
     *
     * @return operation entered from keyboard
     */
    public static CalculatorOperation readOperationFromKeyboard() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter operation (+, -, *, /): ");
        char operation = scanner.next().trim().charAt(0);

        switch (operation) {
            case '+':
                return CalculatorOperation.ADDITION;
            case '-':
                return CalculatorOperation.SUBTRACTION;
            case '*':
                return CalculatorOperation.MULTIPLICATION;
            case '/':
                return CalculatorOperation.DIVISION;
            default:
                System.out.println("This operation cannot be processed. Only the following operations are allowed: +, -, *, /.\n" +
                        "Please try again");
                return readOperationFromKeyboard();
        }
    }

    /**
     * Read a number from keyboard to use for calculation
     *
     * @param numberPosition - the position of the number in the calculated expression
     * @return a double value
     */
    public static double readNumberFromKeyboard(String numberPosition) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter " + numberPosition + " number: ");

        while (!scanner.hasNextDouble()) {
            System.out.println("The entered value is not a number. Please enter a number ");
            scanner.next();
        }
        return scanner.nextDouble();
    }

    /**
     * Continue calculation using the received result from the previous calculation
     *
     * @param currentResult - result of the previous calculation
     * @return true if continue
     */
    public static boolean isContinuingCalculation(double currentResult) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Do you want to continue using the result " + currentResult + "(Y/N)? ");
        char answer = sc.next().trim().toUpperCase().charAt(0);
        if (answer == 'Y') {
            return true;
        } else if (answer == 'N') {
            return false;
        } else {
            System.out.println("Only Y or N answers are allowed. Please try again.");
            return isContinuingCalculation(currentResult);
        }
    }
}