package com.epam.atm.calculator;

public enum CalculatorOperation {
    ADDITION,
    SUBTRACTION,
    MULTIPLICATION,
    DIVISION
}
