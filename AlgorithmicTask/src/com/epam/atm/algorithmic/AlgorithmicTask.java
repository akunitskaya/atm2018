package com.epam.atm.algorithmic;

import java.util.Scanner;

public class AlgorithmicTask {

    public static final int ARRAY_LENGTH = 5;

    public static void main(String[] args) {
        String[] array = new String[ARRAY_LENGTH];
        enterStringArrayFromKeyboard(array);
        String secondToMax = getSecondToMaxString(array);
        System.out.println("Second to max string = " + secondToMax);
    }

    public static void enterStringArrayFromKeyboard(String[] array) {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < array.length; i++) {
            System.out.print("Enter string: ");
            array[i] = scanner.nextLine();
        }
    }

    public static String getSecondToMaxString(String[] array) {
        int maxLength = 0;
        int maxIndex = 0;
        int secondToMaxLength = 0;
        int secondToMaxIndex = 0;

        for (int i = 0; i < array.length; i++) {
            if (maxLength < array[i].length()) {
                /*
                If the second to max string is located before the longest one;
                Once a new longest string is found, the previous longest string becomes the second max string.
                */
                secondToMaxLength = maxLength;
                secondToMaxIndex = maxIndex;
                maxLength = array[i].length();
                maxIndex = i;
            } else if (secondToMaxLength < array[i].length() && array[i].length() != maxLength) {
                /*
                If the second to max string is located after the longest one &&
                additional check in case there are two longest strings in the array.

                Once the next string's length is bigger than the current second to max length and shorter than
                the max string length, then this string is the new second to max string.
                */
                secondToMaxLength = array[i].length();
                secondToMaxIndex = i;
            }
        }

        return array[secondToMaxIndex];
    }
}
