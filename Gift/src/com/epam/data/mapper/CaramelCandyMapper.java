package com.epam.data.mapper;

import com.epam.data.entity.candy.CaramelCandyEntity;
import com.epam.domain.candy.CaramelCandy;

public class CaramelCandyMapper extends Mapper <CaramelCandy, CaramelCandyEntity> {
    @Override
    public CaramelCandy map(CaramelCandyEntity caramelCandyEntity) {
        return new CaramelCandy.CaramelCandyBuilder()
                .price(caramelCandyEntity.getPrice())
                .countryOfOrigin(caramelCandyEntity.getCountryOfOrigin())
                .weight(caramelCandyEntity.getWeight())
                .size(caramelCandyEntity.getSize())
                .flavor(caramelCandyEntity.getFlavor())
                .isLollipop(caramelCandyEntity.isLollipop())
                .build();
    }
}
