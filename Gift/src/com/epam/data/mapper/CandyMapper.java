package com.epam.data.mapper;

import com.epam.data.entity.candy.CandyEntity;
import com.epam.domain.candy.Candy;

public class CandyMapper extends Mapper<Candy, CandyEntity> {
    @Override
    public Candy map(CandyEntity candyEntity) {
        return new Candy.CandyBuilder()
                .price(candyEntity.getPrice())
                .countryOfOrigin(candyEntity.getCountryOfOrigin())
                .weight(candyEntity.getWeight())
                .size(candyEntity.getSize())
                .build();
    }
}
