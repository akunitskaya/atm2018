package com.epam.data.entity.candy;

import com.epam.data.entity.SweetEntity;
import com.epam.domain.candy.CandySize;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Candy")
@XmlAccessorType(XmlAccessType.FIELD)
public class CandyEntity extends SweetEntity {

    @XmlElement(name = "Size")
    private CandySize size;

    public CandySize getSize() {
        return size;
    }

    public void setSize(CandySize size) {
        this.size = size;
    }
}
