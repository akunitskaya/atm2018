package com.epam.data;

import com.epam.domain.Sweet;

import java.util.List;


public interface BaseRepository {
    List<Sweet> readSweets();
}
