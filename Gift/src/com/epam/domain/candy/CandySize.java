package com.epam.domain.candy;

public enum CandySize {
    SMALL, MEDIUM, BIG
}
