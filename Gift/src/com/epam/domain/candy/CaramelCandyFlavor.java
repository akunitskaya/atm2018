package com.epam.domain.candy;

public enum CaramelCandyFlavor {
    VANILLA, CHOCOLATE
}
