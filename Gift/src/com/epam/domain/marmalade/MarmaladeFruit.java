package com.epam.domain.marmalade;

public enum MarmaladeFruit {
    ORANGE, APPLE, CHERRY
}
