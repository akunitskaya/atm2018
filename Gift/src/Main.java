import com.epam.data.BaseRepository;
import com.epam.data.SweetsXmlRepository;
import com.epam.domain.Gift;
import com.epam.domain.Sweet;
import com.epam.domain.SweetCountryOfOrigin;
import com.epam.domain.PreparedSweetFactory;
import com.epam.domain.candy.CaramelCandy;
import com.epam.domain.candy.ChocolateCandy;
import com.epam.domain.chocolate.DarkChocolate;
import com.epam.domain.chocolate.MilkChocolate;
import com.epam.domain.marmalade.Marmalade;


import java.util.List;

public class Main {
    public static void main(String[] args) {

        Sweet caramelCandy = PreparedSweetFactory.getPreparedSweet(CaramelCandy.class);
        Sweet chocolateCandy = PreparedSweetFactory.getPreparedSweet(ChocolateCandy.class);
        Sweet darkChocolate = PreparedSweetFactory.getPreparedSweet(DarkChocolate.class);
        Sweet milkChocolate = PreparedSweetFactory.getPreparedSweet(MilkChocolate.class);
        Sweet marmalade = PreparedSweetFactory.getPreparedSweet(Marmalade.class);

        Gift gift = new Gift();
        gift.putSweets(caramelCandy, chocolateCandy, darkChocolate, milkChocolate, marmalade);

        double giftWeight = gift.getWeight();

        List sortedSweets = gift.getSweetsSorted();

        Sweet sweetWithParameters = gift.getSweetWithParameters(30, 60, SweetCountryOfOrigin.BELGIUM);

        List<Sweet> sweetsFromXMLStorage;
        BaseRepository repo = new SweetsXmlRepository();
        sweetsFromXMLStorage = repo.readSweets();

        for (Sweet sweet : sweetsFromXMLStorage) {
            System.out.println(sweet.toString());
        }
    }
}
